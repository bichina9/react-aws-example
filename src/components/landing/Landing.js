/**
 * Created by User on 29.06.2017.
 */
import React, { Component } from 'react';
import {browserHistory} from 'react-router'
import Cognito from '../../utils/auth'
import StateStorage from '../../utils/stateStorageWorker'

class Landing extends Component {
    state = {identityResponse: null, identityIamResponse: null};
    auth = Cognito.getInstance();
    uid = 'landing';

    componentDidMount() {
        if (StateStorage.restoreState(this, this.uid)) return;
        this.auth.getIdentityIam((res)=>{
            StateStorage.setAndStoreState(this, this.uid ,{identityIamResponse: res.data})
        }, () => {});
        this.auth.getIdentity((res) => {
            StateStorage.setAndStoreState(this, this.uid ,{identityResponse: res.data})
        }, () => {})
    }

    performLogout() {
        this.auth.performLogout();
        StateStorage.setAndStoreState(this, this.uid, {identityResponse: null});
        browserHistory.push('/');
    }

    render() {
        let { identityResponse, identityIamResponse } = this.state;
        return (
        <div>
            <button onClick={this.performLogout.bind(this)} >Log Out</button>
            <br /><br />
            {identityResponse ? JSON.stringify(identityResponse) : null}
            <br /><br />
            {identityIamResponse ? JSON.stringify(identityIamResponse) : null}
        </div>
        );
    }
}

export default Landing;