import React, {Component} from 'react';
import {browserHistory} from 'react-router'
import Cognito from '../../utils/auth'
import './Login.css'

class Login extends Component {
    auth = Cognito.getInstance();
    state = {};

    constructor (props) {
        super(props);
        this.state = {username: '', password: ''};
    }

    loginSuccessful() {
        browserHistory.push('/landing');
    }

    performLogin(e) {
        e.preventDefault();
        this.auth.performLogin({username: this.state.username, password: this.state.password},
            this.loginSuccessful.bind(this), (err) => {alert("Unable to login, possibly due to invalid credentials", err)});
    }

    handleInputChange(name, e) {
        this.setState({[name]: e.target.value})
    }

    render() {
        return (
            <form onSubmit={this.performLogin.bind(this)}>
                <input type="text" name="login" value={this.state.username}
                       placeholder="username"
                       onChange={this.handleInputChange.bind(this,"username")}/>
                <br/>
                <input type="password" name="pass" value={this.state.password}
                       placeholder="password"
                       onChange={this.handleInputChange.bind(this,"password")}/>
                <br/>
                <input type="submit" value="login"/>

            </form>
        );
    }
}

export default Login;