import React from 'react';
import { render } from 'react-dom'
import Login from './components/login/Login';
import Landing from './components/landing/Landing';
import Auth from './utils/auth'
import './index.css';

import { Route, Router, browserHistory } from 'react-router'

let authInstance = Auth.getInstance();

let checkIfLogged = (nextState, transition) => {
    if (!authInstance.isLogged())  {
        transition('/');
    }
};

render((
    <Router history={browserHistory}>
        <Route path='/landing' component={Landing} onEnter={checkIfLogged}/>
        <Route path='/' component={Login} />
    </Router>
), document.getElementById('root'));