/**
 * Created by User on 29.06.2017.
 */

import config from '../config/credentials'

class Cognito  {
    static instance = null;
    apigClient = null;
    cognitoUser = null;
    AWSCognito = window.AWSCognito;
    AWS = window.AWS;
    userPool = null;
    cognitoIdentityUrl = [`cognito-idp.${config.region}.amazonaws.com/${config.UserPoolId}`];

    isLogged() {
        return !!this.cognitoUser
    }

    performLogout() {
        if (this.cognitoUser) {
            this.cognitoUser.signOut();
            this.AWS.config.credentials.clearCachedId();
            this.cognitoUser = null;
        }
        window.localStorage.removeItem('awsCredentials')
    }

    static getInstance() {
        if (!this.instance) {
            this.instance = new this();
        }
        return this.instance;
    }

    constructor() {
        this.AWSCognito.config = config;
        this.AWS.config = config;
        this.AWS.config.credentials = new this.AWS.CognitoIdentityCredentials(config);
        this.userPool = new this.AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(config);
        this.cognitoUser = this.userPool.getCurrentUser();
    }

    getIdentity(onSuccess, onError) {
        this.apigClient = window.apigClientFactory.newClient();
        this.cognitoUser.getSession((err,ses) => {
            this.apigClient.identityGet({},{}, {
                headers: {
                    Authorization: ses.idToken.getJwtToken()
                }
            }).then(onSuccess).catch(onError)
        })
    }

    getIdentityIam(onSuccess, onError) {
        let creds = JSON.parse(window.localStorage.getItem('awsCredentials'));
        this.apigClient = window.apigClientFactory.newClient(creds);
        if (!this.apigClient) {
            return
        }
        this.apigClient.identityiamGet(null, null, {})
            .then(onSuccess).catch(onError)
    }

    prepareAWSCredentials(creds) {
        return {
            accessKey: creds.accessKeyId,
            secretKey: creds.secretAccessKey,
            sessionToken: creds.sessionToken,
            region: config.region
        }
    }

    performLogin(creds, onSuccess, onError) {
        let { username, password } = creds;
        let authenticationData = {
            Username : username,
            Password : password,
        };
        let authenticationDetails = new this.AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);
        let userData = {
            Username : username,
            Pool : this.userPool
        };
        this.cognitoUser = new this.AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        this.cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: (res) => {
                this.AWS.config.region = config.region;
                config.Logins =
                {
                    [this.cognitoIdentityUrl] :res.getIdToken().getJwtToken()
                };
                this.AWS.config.credentials.get(() => {
                    let creds = this.prepareAWSCredentials(this.AWS.config.credentials);
                    window.localStorage.setItem('awsCredentials', JSON.stringify(creds));
                    onSuccess(creds);
                });
            },
            onFailure: (err) => {
                onError(err);
            }
        });
    };
}

export default Cognito;
