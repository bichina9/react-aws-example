class StateStorage {
    static setAndStoreState = (self, uid, state) => {
        let old = self.state;
        self.setState(state, () => {
            let newState = Object.assign({}, old, state);
            window.localStorage.removeItem(uid);
            window.localStorage.setItem(uid, JSON.stringify(newState));
            return newState;
        })
    };

    static restoreState = (self, uid) => {
        let state = window.localStorage.getItem(uid);
        if (state) {
            self.setState(JSON.parse(state))
        }
        return !!state;
    };
}

export default StateStorage